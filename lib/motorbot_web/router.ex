defmodule MotorbotWeb.Router do
  use MotorbotWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/admin", MotorbotWeb.Admin, as: :admin do
    pipe_through :browser # Use the default browser stack
    get "/", DashboardController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", MotorbotWeb do
  #   pipe_through :api
  # end
end
