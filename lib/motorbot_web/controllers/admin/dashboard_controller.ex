defmodule MotorbotWeb.Admin.DashboardController do
  use MotorbotWeb, :controller
  def show(conn, _params) do
    render conn, "show.html"
  end
end
