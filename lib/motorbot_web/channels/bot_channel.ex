defmodule MotorbotWeb.BotChannel do
  use MotorbotWeb, :channel
  def join("pos", payload, socket) do
    welcome_text = "Hello! Welcome to Motorbot Project"
    {:ok, %{message: welcome_text}, socket}
  end
  def handle_in(message, payload, socket) do
    reply = %{ message: message }
    {:reply, {:ok, reply}, socket}
  end
end
