# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :motorbot,
  ecto_repos: [Motorbot.Repo]

# Configures the endpoint
config :motorbot, MotorbotWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "RdSd+Ftgu5LRkI4YCE4RxIU2TMQb+2uUUUz6wEStvzE8lMeZ5IPOeXxE+76DKzvq",
  render_errors: [view: MotorbotWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Motorbot.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
